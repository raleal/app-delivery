/* Creamos algunos usuarios con sus roles */
INSERT INTO role (role, status) VALUES ('ROLE_USER', 1);
INSERT INTO role (role, status) VALUES ('ROLE_ADMIN', 1);

INSERT INTO users (full_name, username, password, status, role_id) VALUES ('Jhon Doe','user@d.com','$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG',1,1);
INSERT INTO users (full_name, username, password, status, role_id) VALUES ('Mary Jane','admin@d.com','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1,2);

INSERT INTO bank (name, status) VALUES ('Banesco', 1);
INSERT INTO bank (name, status) VALUES ('Banco Mercantil', 1);
INSERT INTO bank (name, status) VALUES ('Banco de Venezuela', 1);

INSERT INTO type_identification (name, status) VALUES ('J', 1);
INSERT INTO type_identification (name, status) VALUES ('V', 1);
INSERT INTO type_identification (name, status) VALUES ('E', 1);

INSERT INTO brand (name, status) VALUES ('P.A.N', 1);
INSERT INTO brand (name, status) VALUES ('Juana', 1);
INSERT INTO brand (name, status) VALUES ('Kaly', 1);
INSERT INTO brand (name, status) VALUES ('Primor', 1);
INSERT INTO brand (name, status) VALUES ('Ronco', 1);
INSERT INTO brand (name, status) VALUES ('Mavesa', 1);

INSERT INTO category (name, status) VALUES ('Charcuteria', 1);
INSERT INTO category (name, status) VALUES ('Cereales', 1);
INSERT INTO category (name, status) VALUES ('Perfumeria', 1);
INSERT INTO category (name, status) VALUES ('Lacteos', 1);
INSERT INTO category (name, status) VALUES ('Otro', 1);

INSERT INTO currency (name, symbol, value, status) VALUES ('Dolar', '$', 110000.00, 1);
INSERT INTO currency (name, symbol, value, status) VALUES ('Bolivares', 'Bs', 1.00, 1);

INSERT INTO product (name, category_id, status) VALUES ('Harina', 5, 1);
INSERT INTO product (name, category_id, status) VALUES ('Arroz', 2, 1);
INSERT INTO product (name, category_id, status) VALUES ('Pasta', 5, 1);
INSERT INTO product (name, category_id, status) VALUES ('Mantequilla', 5, 1);
INSERT INTO product (name, category_id, status) VALUES ('Mayonesa', 5, 1);
INSERT INTO product (name, category_id, status) VALUES ('Leche', 4, 1);

INSERT INTO type_account (name, status) VALUES ('Ahorros', 1);
INSERT INTO type_account (name, status) VALUES ('Corriente', 1);
INSERT INTO type_account (name, status) VALUES ('Pago Movil', 1);

INSERT INTO unit (name, status) VALUES ('Gramos', 1);
INSERT INTO unit (name, status) VALUES ('Kilogramos', 1);
INSERT INTO unit (name, status) VALUES ('Mililitros', 1);
INSERT INTO unit (name, status) VALUES ('Litros', 1);
INSERT INTO unit (name, status) VALUES ('Unidades', 1);

INSERT INTO headline (name, mts, status) VALUES ('100 mts', 100, 1);
INSERT INTO headline (name, mts, status) VALUES ('500 mts', 500, 1);
INSERT INTO headline (name, mts, status) VALUES ('1 Km', 1000, 1);
INSERT INTO headline (name, mts, status) VALUES ('2 Km', 2000, 1);
INSERT INTO headline (name, mts, status) VALUES ('3 Km', 3000, 1);
INSERT INTO headline (name, mts, status) VALUES ('4 Km', 4000, 1);
INSERT INTO headline (name, mts, status) VALUES ('5 Km', 5000, 1);

INSERT INTO time_delivery (name, status) VALUES ('1 Hora', 1);
INSERT INTO time_delivery (name, status) VALUES ('4 Horas', 1);
INSERT INTO time_delivery (name, status) VALUES ('8 Horas', 1);
INSERT INTO time_delivery (name, status) VALUES ('1 Día', 1);
INSERT INTO time_delivery (name, status) VALUES ('2 Días', 1);

INSERT INTO week (name, abbreviation, status) VALUES ('Lunes', 'Lun', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Martes', 'Mar', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Miercoles', 'Mie', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Jueves', 'Jue', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Viernes', 'Vie', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Sabado', 'Sab', 1);
INSERT INTO week (name, abbreviation, status) VALUES ('Domingo', 'Dom', 1);
