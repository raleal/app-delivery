package com.app.delivery;


import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class GenerateCodeQR {
	
	

    public void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {
    	//System.out.println("text "+text+" filePath "+filePath);
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
        
        String c = "chmod 644 "+filePath+"";

        try {
        Runtime.getRuntime().exec(c);
        } catch (IOException ex) {
        //Logger.getLogger(UploadPhotoToCategory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

}
