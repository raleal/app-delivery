package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/commerce")
public class CommerceController {

	@Autowired
	private ICommerceService iCommerceService;
	
	@Autowired
	private IUsersService iUsersService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;	

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Commerce> listar() {
		return iCommerceService.commerceActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody List<Commerce> commerce, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*
[
    {
        "id": 1,
        "name": "Inversiones X",
        "identification": "114785236",
        "address": "Caracas",
        "latitude": "120.36.25.14",
        "longitude": "130.95.75.15",
        "urlImage": null,
        "startTime": "08:00:00",
        "endTime": "18:00:00",
        "headline": {
            "id": 2,
            "name": "500 mts",
            "mts": 500.0,
            "status": true
        },
        "timeDelivery": {
            "id": 4,
            "name": "1 Día",
            "status": true
        },
        "status": true,
        "typeIdentification": {
            "id": 1,
            "name": "J",
            "status": true
        },
        "users": {
            "id": 2,
            "fullName": "Mary Jane",
            "username": "admin@d.com",
            "password": "$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS",
            "identification": null,
            "urlImage": null,
            "token": null,
            "status": true,
            "createdDate": null,
            "updatedDate": null,
            "role": {
                "id": 2,
                "role": "ROLE_ADMIN",
                "status": true
            },
            "typeIdentification": null
        },
        "createdDate": "2020-04-14 11:38:22",
        "updatedDate": null
    }
]
		 */

		HashMap<String, String> map = new HashMap<>();

		try {

			for(int i=0;i<commerce.size();i++) {
				commerce.get(i).setStatus(true);
			}
			
			iCommerceService.save(commerce);
			map.put("mensaje", "Guardado con Éxito");

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
	
	@GetMapping(value = "/request/{id}")
	@Secured({ "ROLE_ADMIN", "ROLE_USER"})
	public Map<String, String> request(@PathVariable(value = "id") Long id, HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException, WriterException {

		HashMap<String, String> map = new HashMap<>();
		Commerce commerce = new Commerce();
		Users user = new Users();
		GenerateCodeQR codeQR = new GenerateCodeQR();

		//try {
			
			user = iUsersService.findByUsername(authentication.getPrincipal().toString());
			commerce = iCommerceService.commerce(id);

			if (user != null) {
				
				Integer randomNum = ThreadLocalRandom.current().nextInt(100000, 900000);
				//System.out.println("randomNum "+randomNum);
				String token = passwordEncoder.encode(randomNum.toString());
				//System.out.println("bcryptCode "+token);
				user.setToken(token);
				
				iUsersService.updateToken(token, user.getId());

				String QR_CODE_PATH = "C:/Users/ronny/QR/";
				//String QR_CODE_PATH = "/home/amplyflyer/public_html/images/codes/";
	        	String QR_CODE_IMAGE_PATH = "";

				QR_CODE_IMAGE_PATH = QR_CODE_PATH + user.getUsername() + "~"
						+ System.currentTimeMillis() + ".png";
				// generateQRCodeImage(String text, int width, int height, String filePath)
				codeQR.generateQRCodeImage(user.getId()+"~|~"+user.getToken()+"~|~"+commerce.getId(), 320, 350, QR_CODE_IMAGE_PATH);
				//System.out.println(" ---CODE QR--- "+QR_CODE_IMAGE_PATH);
				System.out.println("user.getId()+user.getToken()+commerce.getId() "+user.getId()+"~|~"+user.getToken()+"~|~"+commerce.getId());
				
				map.put("mensaje", "Guardado con Éxito");
				map.put("urlImage", QR_CODE_IMAGE_PATH);
				
				return map;
				
			} else {
				
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Usuario no esta disponible");
				return map;
				
			}
			
		/*} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error consultando los datos");

			return map;
		}*/

	}
}
