package com.app.delivery.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserAccount;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUserAccountService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.app.delivery.GenerateCodeQR;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/userAccount")
public class UserAccountController {

	@Autowired
	private IUserAccountService iUserAccountService;
	
	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<UserAccount> list() {
		return iUserAccountService.userAccountActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody List<UserAccount> userAccount, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*[
    {
        "id": 1,
        "number": "01348523698745214569",
        "identification": "18461461",
        "holder": "Juan Perez",
        "phone": "+584248965214",
        "movilPay": null,
        "status": true,
        "createdDate": "2020-04-08 13:58:39",
        "updatedDate": null,
        "bank": {
            "id": 1,
            "name": "Banesco",
            "status": true
        },
        "typeIdentification": {
            "id": 2,
            "name": "V",
            "status": true
        },
        "typeAccount": {
            "id": 1,
            "name": "Ahorros",
            "status": true
        },
        "users": {
            "id": 2,
            "fullName": "Mary Jane",
            "username": "admin@d.com",
            "password": "$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS",
            "identification": null,
            "urlImage": null,
            "token": null,
            "status": true,
            "createdDate": "2020-04-08 00:00:00",
            "updatedDate": null,
            "role": {
                "id": 2,
                "role": "ROLE_ADMIN",
                "status": true
            },
            "typeIdentification": null
        }
    }
]*/

		HashMap<String, String> map = new HashMap<>();
		//GenerateCodeQR codeQR = new GenerateCodeQR();

		try {
			
			for(int i=0;i<userAccount.size();i++) {
				userAccount.get(i).setStatus(true);
			}
			
			iUserAccountService.save(userAccount);
			map.put("mensaje", "Guardado con Éxito");
			
			/*Email sendEmail = new Email();
			sendEmail.welcomeMail(users.getUsername(), users.getName()+" "+users.getLastName(), "", "");*/

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
