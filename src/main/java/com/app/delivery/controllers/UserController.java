package com.app.delivery.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.app.delivery.GenerateCodeQR;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private IUsersService iUsersService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Users> list() {
		return iUsersService.usersActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody Users user, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*    {
        "id": 1,
        "fullName": "Jhon Doe",
        "username": "user@d.com",
        "password": "$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG",
        "identification": null,
        "urlImage": null,
        "token": null,
        "status": true,
        "createdDate": "2020-04-08 00:00:00",
        "updatedDate": null,
        "role": {
            "id": 1,
            "role": "ROLE_USER",
            "status": true
        },
        "typeIdentification": null
    }*/

		HashMap<String, String> map = new HashMap<>();
		//GenerateCodeQR codeQR = new GenerateCodeQR();

		try {
			
			/*Integer randomNum = ThreadLocalRandom.current().nextInt(100000, 900000);
			//System.out.println("randomNum "+randomNum);
			String token = passwordEncoder.encode(randomNum.toString());
			//System.out.println("bcryptCode "+token);
			user.setToken(token);*/
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setStatus(true);
			iUsersService.save(user);
			map.put("mensaje", "Guardado con Éxito");
			
			/*Email sendEmail = new Email();
			sendEmail.welcomeMail(users.getUsername(), users.getName()+" "+users.getLastName(), "", "");*/

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
