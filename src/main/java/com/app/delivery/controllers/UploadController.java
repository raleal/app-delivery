package com.app.delivery.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUploadFileService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.app.delivery.GenerateCodeQR;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/upload")
public class UploadController {

	@Autowired
	private IUploadFileService iUploadFileService;

	@Secured({ "ROLE_ADMIN" })
	@PostMapping(value = "/transfer")
	public Map<String, String> transfer(@RequestParam("transfer") MultipartFile transfer, HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		HashMap<String, String> map = new HashMap<>();
		String fileName = "";

		try {

			if (!transfer.isEmpty()) {
				try {
					fileName = iUploadFileService.copy(transfer);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			map.put("mensaje", "Guardado con Éxito");
			map.put("urlImage", fileName);
			
			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
