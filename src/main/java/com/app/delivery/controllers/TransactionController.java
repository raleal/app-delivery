package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Transaction;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.ITransactionService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

	@Autowired
	private ITransactionService iTransactionService;

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Transaction> listar() {
		return iTransactionService.transactionActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN", "ROLE_USER"})
	public Map<String, String> save(Map<String, String[]> json, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {
		 System.out.println("TESTTTTTTTTTTTTTTTT");
		HashMap<String, String> map = new HashMap<>();
		
		try {
			
			HashMap<String, String> map2 = new HashMap<>();
			json = request.getParameterMap();
			System.out.println("json "+json);
			//map2 = request.getParameterMap();
			String[] numContrato = request.getParameter("code").toString().split("~|~");
			System.out.println("numContrato.length " + numContrato.length);

			/*iTransactionService.save(transaction);
			map.put("mensaje", "Guardado con Éxito");

			Email sendEmail = new Email();
			sendEmail.welcomeMail(commerce.getEmail(), commerce.getFullName(), commerce.getNameCommerce(),
					commerce.getIdentification());*/

			return map;

		} catch (Exception e) { // TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}

	/*
	 * @GetMapping(value = "/save/{code}")
	 * 
	 * @Secured({ "ROLE_ADMIN", "ROLE_USER" }) public Map<String, String>
	 * request(@PathVariable(value = "code") String code, HttpServletRequest
	 * request, HttpServletResponse response, Authentication authentication) throws
	 * IOException, WriterException { System.out.println("TESTTTTTTTTTTTTTTTT");
	 * HashMap<String, String> map = new HashMap<>(); Commerce commerce = new
	 * Commerce(); Users user = new Users(); GenerateCodeQR codeQR = new
	 * GenerateCodeQR();
	 * 
	 * // try {
	 * 
	 * String[] numContrato = code.split("~|~");
	 * System.out.println("numContrato.length " + numContrato.length);
	 * 
	 * user =
	 * iUsersService.findByUsername(authentication.getPrincipal().toString());
	 * commerce = iCommerceService.commerce(id);
	 * 
	 * if (user != null) {
	 * 
	 * String QR_CODE_PATH = "C:/Users/ronny/QR/"; //String QR_CODE_PATH =
	 * "/home/amplyflyer/public_html/images/codes/"; String QR_CODE_IMAGE_PATH = "";
	 * 
	 * QR_CODE_IMAGE_PATH = QR_CODE_PATH + user.getUsername() + "~" +
	 * System.currentTimeMillis() + ".png"; // generateQRCodeImage(String text, int
	 * width, int height, String filePath)
	 * codeQR.generateQRCodeImage(user.getId()+"~|~"+user.getToken()+"~|~"+commerce.
	 * getId(), 320, 350, QR_CODE_IMAGE_PATH);
	 * //System.out.println(" ---CODE QR--- "+QR_CODE_IMAGE_PATH);
	 * System.out.println("user.getId()+user.getToken()+commerce.getId() "+user.
	 * getId()+"~|~"+user.getToken()+"~|~"+commerce.getId());
	 * 
	 * Integer randomNum = ThreadLocalRandom.current().nextInt(100000, 900000);
	 * //System.out.println("randomNum "+randomNum); String token =
	 * passwordEncoder.encode(randomNum.toString());
	 * //System.out.println("bcryptCode "+token); user.setToken(token);
	 * 
	 * iUsersService.updateToken(token, user.getId());
	 * 
	 * map.put("mensaje", "Guardado con Éxito"); map.put("urlImage",
	 * QR_CODE_IMAGE_PATH);
	 * 
	 * 
	 * return map;
	 * 
	 * 
	 * } else {
	 * 
	 * response.sendError(HttpServletResponse.SC_BAD_REQUEST,
	 * "Comercio no esta disponible"); return map;
	 * 
	 * }
	 * 
	 * 
	 * 
	 * } catch (Exception e) { // TODO: handle exception
	 * response.sendError(HttpServletResponse.SC_BAD_REQUEST,
	 * "Error consultando los datos");
	 * 
	 * return map; }
	 * 
	 * 
	 * }
	 */
}
