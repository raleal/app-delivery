package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Stock;
import com.app.delivery.models.entity.UserStock;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IStockService;
import com.app.delivery.models.service.IUserStockService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/userStock")
public class UserStockController {

	@Autowired
	private IStockService iStockService;
		
	@Autowired
	private IUserStockService iUserStockService;
	
	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<UserStock> list() {
		return iUserStockService.userStockActive();
	}

	@RequestMapping(value = "/add")
	@Secured({ "ROLE_ADMIN", "ROLE_USER"})
	public Map<String, String> add(@RequestBody UserStock userStock, HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException, WriterException {

		HashMap<String, String> map = new HashMap<>();
		Stock stock = new Stock();
		UserStock userStockDB = new UserStock();
		//Users user = new Users();

		try {
			
			stock = iStockService.stock(userStock.getStock().getId());

			if (stock != null) {
				
				if (stock.getCount() >= userStock.getCount()) {
					
					Integer status = iStockService.updateStock(userStock.getCount(), userStock.getStock().getId());
					if(status!=0) {
						
						System.out.println("status "+status);
						
						userStockDB = iUserStockService.userStockDB(userStock.getStock().getId());
						userStock.setStatus(true);
						iUserStockService.save(userStock);
						
						map.put("mensaje", "Guardado con Éxito");
						
					} else {
						System.out.println("status "+status);
						response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Cantidad de stock ya no esta disponible");
						return map;
						
					}
					
					
				} else {
					
					if(stock.getCount()>0) {
						response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Cantidad de stock ya no esta disponible, solo quedan "+stock.getCount());
						return map;
					} else {
						response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Cantidad de stock ya no esta disponible");
						return map;
					}
					
					
				}
				
				return map;
				
			} else {
				
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Stock no esta disponible");
				return map;
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error consultando los datos");

			return map;
		}

	}
	
}
