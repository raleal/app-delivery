package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Delivery;
import com.app.delivery.models.entity.ShoppingCart;
import com.app.delivery.models.entity.UserStock;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.IDeliveryService;
import com.app.delivery.models.service.IUserStockService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/delivery")
public class DeliveryController {

	@Autowired
	private IDeliveryService iDeliveryService;
	
	@Autowired
	private IUsersService iUsersService;
	
	@Autowired
	private IUserStockService iUsersStockService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;	

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Delivery> listar() {
		return iDeliveryService.deliveryActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody Delivery delivery, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*
    {
        "id": 1,
        "description": "Compra",
        "latitude": "230.25.36.98",
        "longitude": "125.87.96.65",
        "urlImage": null,
        "transfer_number": "2587469856214",
        "sent": null,
        "received": null,
        "status": true,
        "usersAccount": {
            "id": 1,
            "number": "01348523698745214569",
            "identification": "18461461",
            "holder": "Juan Perez",
            "phone": "+584248965214",
            "movilPay": null,
            "status": true,
            "createdDate": "2020-04-17 13:38:41",
            "updatedDate": null,
            "bank": {
                "id": 1,
                "name": "Banesco",
                "status": true
            },
            "typeIdentification": {
                "id": 2,
                "name": "V",
                "status": true
            },
            "typeAccount": {
                "id": 1,
                "name": "Ahorros",
                "status": true
            },
            "users": {
                "id": 2,
                "fullName": "Mary Jane",
                "username": "admin@d.com",
                "password": "$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS",
                "identification": null,
                "urlImage": null,
                "token": null,
                "status": true,
                "createdDate": null,
                "updatedDate": null,
                "role": {
                    "id": 2,
                    "role": "ROLE_ADMIN",
                    "status": true
                },
                "typeIdentification": null
            }
        },
        "shoppingCart": [],
        "createdDate": "2020-04-09 00:00:00",
        "updatedDate": null
    }
		 */

		HashMap<String, String> map = new HashMap<>();
		List<ShoppingCart> listShoppingCart = new ArrayList<ShoppingCart>();
		ShoppingCart shoppingCart = new ShoppingCart();

		try {
			if (!(delivery.getSent() != null || delivery.getReceived() != null)
					|| delivery.getShoppingCart().size() > 0) {
				
				List<UserStock> userStock = iUsersStockService.userStock(delivery.getUsers().getId());
				
				for (int i = 0; i < userStock.size(); i++) {
					shoppingCart.setCount(userStock.get(i).getCount());
					shoppingCart.setStock(userStock.get(i).getStock());
					shoppingCart.setStatus(true);
					
					listShoppingCart.add(shoppingCart);
				}
				
				delivery.setShoppingCart(listShoppingCart);

			}
			delivery.setStatus(true);
			iDeliveryService.save(delivery);
			map.put("mensaje", "Guardado con Éxito");

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
