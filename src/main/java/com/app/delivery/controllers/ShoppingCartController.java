package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.ShoppingCart;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IDeliveryService;
import com.app.delivery.models.service.IShoppingCartService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/shoppingCart")
public class ShoppingCartController {

	@Autowired
	private IShoppingCartService iShoppingCartService;
	
	@Autowired
	private IDeliveryService iDeliveryService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;	

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<ShoppingCart> list() {
		return iShoppingCartService.shoppingCartActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody List<ShoppingCart> shoppingCart, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/**/

		HashMap<String, String> map = new HashMap<>();

		try {
			
			for(int i=0;i<shoppingCart.size();i++) {
				shoppingCart.get(i).setStatus(true);
			}

			iShoppingCartService.save(shoppingCart);
			map.put("mensaje", "Guardado con Éxito");

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
	
}
