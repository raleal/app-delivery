package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Product;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.IProductService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/product")
public class ProductController {

	@Autowired
	private IProductService iProductService;
	
	@Autowired
	private IUsersService iUsersService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;	

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Product> listar() {
		return iProductService.productActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody Product product, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*
    {
        "id": 1,
        "name": "Harina",
        "status": true,
        "createdDate": null,
        "updatedDate": null,
        "category": {
            "id": 5,
            "name": "Otro",
            "status": true,
            "createdDate": null,
            "updatedDate": null
        }
    }
		 */

		HashMap<String, String> map = new HashMap<>();

		try {

			product.setStatus(true);
			iProductService.save(product);
			map.put("mensaje", "Guardado con Éxito");

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
