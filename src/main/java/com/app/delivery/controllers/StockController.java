package com.app.delivery.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.delivery.GenerateCodeQR;
import com.app.delivery.MyUserDetails;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Stock;
import com.app.delivery.models.entity.UserStock;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IStockService;
import com.app.delivery.models.service.IUserStockService;
import com.app.delivery.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/stock")
public class StockController {

	@Autowired
	private IStockService iStockService;
	
	@Autowired
	private IUsersService iUsersService;
	
	@Autowired
	private IUserStockService iUserStockService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;	

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Stock> list() {
		return iStockService.stockActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody List<Stock> stock, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*
[
    {
        "id": 1,
        "count": 30,
        "content": 1,
        "unit_price": 100000.0,
        "status": true,
        "createdDate": "2020-04-08 00:00:00",
        "updatedDate": null,
        "unit": {
            "id": 2,
            "name": "Kilogramos",
            "status": true
        },
        "commerce": {
            "id": 1,
            "name": "Inversiones X",
            "identification": "114785236",
            "address": "Caracas",
            "latitude": "120.36.25.14",
            "longitude": "130.95.75.15",
            "urlImage": null,
            "status": true,
            "typeIdentification": {
                "id": 1,
                "name": "J",
                "status": true
            },
            "createdDate": "2020-04-08 14:40:02",
            "updatedDate": null
        },
        "product": {
            "id": 1,
            "name": "Harina",
            "status": true,
            "createdDate": null,
            "updatedDate": null,
            "category": {
                "id": 5,
                "name": "Otro",
                "status": true,
                "createdDate": null,
                "updatedDate": null
            }
        },
        "brand": {
            "id": 1,
            "name": "P.A.N",
            "status": true,
            "createdDate": null,
            "updatedDate": null
        },
        "currency": {
            "id": 2,
            "name": "Bolivares",
            "symbol": "Bs",
            "value": 1.0,
            "status": true
        }
    }
]
		 */

		HashMap<String, String> map = new HashMap<>();

		try {
			
			for(int i=0;i<stock.size();i++) {
				stock.get(i).setStatus(true);
			}

			iStockService.save(stock);
			map.put("mensaje", "Guardado con Éxito");

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
	
}
