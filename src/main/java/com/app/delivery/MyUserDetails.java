package com.app.delivery;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.app.delivery.models.entity.Users;

public class MyUserDetails extends org.springframework.security.core.userdetails.User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Users user;

	public MyUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, Users user) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.user = user;
	}
	
	public Users getUser() {
        return user;
    }

}
