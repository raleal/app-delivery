package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;

public interface ICommerceDao extends CrudRepository<Commerce, Long>{
	
	@Query(value = "SELECT * FROM commerce c WHERE c.status = 1 AND c.id = ?1", nativeQuery = true)
	public Commerce commerce(Long id);
	
	@Query(value = "SELECT * FROM commerce c WHERE c.status = 1", nativeQuery = true)
	public List<Commerce> commerceActive();

}
