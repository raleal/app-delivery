package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Transaction;

public interface ITransactionDao extends CrudRepository<Transaction, Long>{
	
	@Query(value = "SELECT * FROM transaction t WHERE t.status = 1 AND t.id = ?1", nativeQuery = true)
	public Transaction transaction(Long id);
	
	@Query(value = "SELECT * FROM transaction t WHERE t.status = 1", nativeQuery = true)
	public List<Transaction> transactionActive();

}
