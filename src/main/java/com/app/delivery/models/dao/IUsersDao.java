package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.app.delivery.models.entity.Users;

public interface IUsersDao extends CrudRepository<Users, Long>{
	
	public Users findByUsername(String username);
	
	@Query(value = "SELECT * FROM users u WHERE u.status = 1", nativeQuery = true)
	public List<Users> usersActive();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE users u SET u.token = ?1 WHERE u.id = ?2", nativeQuery = true)
	public void updateToken(String token, Long id);

}
