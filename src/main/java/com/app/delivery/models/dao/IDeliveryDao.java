package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Brand;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Delivery;
import com.app.delivery.models.entity.Users;

public interface IDeliveryDao extends CrudRepository<Delivery, Long>{
	
	@Query(value = "SELECT * FROM delivery d WHERE d.status = 1 AND d.id = ?1", nativeQuery = true)
	public Delivery delivery(Long id);
	
	@Query(value = "SELECT * FROM delivery d WHERE d.status = 1", nativeQuery = true)
	public List<Delivery> deliveryActive();

}
