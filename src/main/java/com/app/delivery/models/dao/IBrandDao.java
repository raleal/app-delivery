package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Brand;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;

public interface IBrandDao extends CrudRepository<Brand, Long>{
	
	@Query(value = "SELECT * FROM brand b WHERE b.status = 1 AND b.id = ?1", nativeQuery = true)
	public Brand brand(Long id);
	
	@Query(value = "SELECT * FROM brand b WHERE b.status = 1", nativeQuery = true)
	public List<Brand> brandActive();

}
