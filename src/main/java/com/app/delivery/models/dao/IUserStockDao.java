package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserAccount;
import com.app.delivery.models.entity.UserStock;

public interface IUserStockDao extends CrudRepository<UserStock, Long>{
		
	@Query(value = "SELECT * FROM users_stock u WHERE u.status = 1", nativeQuery = true)
	public List<UserStock> userStockActive();
	
	@Query(value = "SELECT * FROM users_stock u WHERE u.status = 1 AND u.users_id = ?1", nativeQuery = true)
	public List<UserStock> userStock(Long id);
	
	@Query(value = "SELECT * FROM users_stock u WHERE u.status = 1 AND u.stock_id = ?1", nativeQuery = true)
	public UserStock userStockDB(Long id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE users_stock u SET u.status = ?1 WHERE u.id = ?2", nativeQuery = true)
	public void inactiveUserStock(Boolean status, Long id);

}
