package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Stock;
import com.app.delivery.models.entity.Users;

public interface IStockDao extends CrudRepository<Stock, Long>{
	
	@Query(value = "SELECT * FROM stock s WHERE s.status = 1 AND s.id = ?1", nativeQuery = true)
	public Stock stock(Long id);
	
	@Query(value = "SELECT * FROM stock s WHERE s.status = 1", nativeQuery = true)
	public List<Stock> stockActive();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE stock s SET s.count = s.count - ?1 WHERE s.id = ?2 AND s.count >= ?1", nativeQuery = true)
	public Integer updateStock(Integer count, Long id);
	

}
