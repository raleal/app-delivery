package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserAccount;

public interface IUserAccountDao extends CrudRepository<UserAccount, Long>{
		
	@Query(value = "SELECT * FROM users_account u WHERE u.status = 1", nativeQuery = true)
	public List<UserAccount> userAccountActive();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE users_account u SET u.status = ?1 WHERE u.id = ?2", nativeQuery = true)
	public void inactiveAccount(Boolean status, Long id);

}
