package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Product;
import com.app.delivery.models.entity.Users;

public interface IProductDao extends CrudRepository<Product, Long>{
	
	@Query(value = "SELECT * FROM product p WHERE p.status = 1 AND p.id = ?1", nativeQuery = true)
	public Product product(Long id);
	
	@Query(value = "SELECT * FROM product p WHERE p.status = 1", nativeQuery = true)
	public List<Product> productActive();

}
