package com.app.delivery.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.delivery.models.entity.Brand;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.ShoppingCart;
import com.app.delivery.models.entity.Users;

public interface IShoppingCartDao extends CrudRepository<ShoppingCart, Long>{
	
	@Query(value = "SELECT * FROM shopping_cart sc WHERE sc.status = 1 AND sc.id = ?1", nativeQuery = true)
	public ShoppingCart shoppingCart(Long id);
	
	@Query(value = "SELECT * FROM shopping_cart sc WHERE sc.status = 1", nativeQuery = true)
	public List<ShoppingCart> shoppingCartActive();

}
