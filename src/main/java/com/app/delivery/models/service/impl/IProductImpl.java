package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.ICommerceDao;
import com.app.delivery.models.dao.IProductDao;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Product;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IProductService;

@Service
public class IProductImpl implements IProductService{
	
	@Autowired
	private IProductDao iProductDao;

	@Override
	public List<Product> findAll() {
		// TODO Auto-generated method stub
		return (List<Product>) iProductDao.findAll();
	}

	@Override
	public void save(Product product) {
		// TODO Auto-generated method stub
		iProductDao.save(product);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Product> productActive() {
		// TODO Auto-generated method stub
		return (List<Product>) iProductDao.productActive();
	}

	@Override
	public Product product(Long id) {
		// TODO Auto-generated method stub
		return iProductDao.product(id);
	}

	

}
