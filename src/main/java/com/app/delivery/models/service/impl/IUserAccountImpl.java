package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.ICommerceDao;
import com.app.delivery.models.dao.IUserAccountDao;
import com.app.delivery.models.dao.IUsersDao;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserAccount;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUserAccountService;
import com.app.delivery.models.service.IUsersService;

@Service
public class IUserAccountImpl implements IUserAccountService{
	
	@Autowired
	private IUserAccountDao iUserAccountDao;
	
	@Override
	public List<UserAccount> findAll() {
		// TODO Auto-generated method stub
		return (List<UserAccount>) iUserAccountDao.findAll();
	}

	@Override
	public void save(List<UserAccount> userAccount) {
		// TODO Auto-generated method stub
		iUserAccountDao.saveAll(userAccount);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<UserAccount> userAccountActive() {
		// TODO Auto-generated method stub
		return (List<UserAccount>) iUserAccountDao.userAccountActive();
	}

	@Override
	public void inactiveAccount(Boolean status, Long id) {
		// TODO Auto-generated method stub
		iUserAccountDao.inactiveAccount(status, id);
	}

}
