package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;

public interface ICommerceService {
	
	public List<Commerce> findAll();
	
	public List<Commerce> commerceActive();
	
	public void save(List<Commerce> commerce);
	
	public void delete(Long id);
	
	public Commerce commerce(Long id);

}
