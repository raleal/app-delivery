package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserAccount;

public interface IUserAccountService {
		
	public List<UserAccount> findAll();
	
	public List<UserAccount> userAccountActive();
	
	public void save(List<UserAccount> userAccount);
	
	public void delete(Long id);
	
	public void inactiveAccount(Boolean status, Long id);

}
