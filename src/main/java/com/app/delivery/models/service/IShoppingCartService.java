package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.ShoppingCart;

public interface IShoppingCartService {
	
	public List<ShoppingCart> findAll();
	
	public List<ShoppingCart> shoppingCartActive();
	
	public void save(List<ShoppingCart> shoppingCart);
	
	public void delete(Long id);
	
	public ShoppingCart shoppingCart(Long id);

}
