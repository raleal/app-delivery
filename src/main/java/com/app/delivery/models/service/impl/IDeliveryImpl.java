package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.IDeliveryDao;
import com.app.delivery.models.entity.Delivery;
import com.app.delivery.models.service.IDeliveryService;

@Service
public class IDeliveryImpl implements IDeliveryService{
	
	@Autowired
	private IDeliveryDao iDeliveryDao;

	@Override
	public List<Delivery> findAll() {
		// TODO Auto-generated method stub
		return (List<Delivery>) iDeliveryDao.findAll();
	}

	@Override
	public void save(Delivery Delivery) {
		// TODO Auto-generated method stub
		iDeliveryDao.save(Delivery);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Delivery> deliveryActive() {
		// TODO Auto-generated method stub
		return (List<Delivery>) iDeliveryDao.deliveryActive();
	}

	@Override
	public Delivery delivery(Long id) {
		// TODO Auto-generated method stub
		return iDeliveryDao.delivery(id);
	}

	

}
