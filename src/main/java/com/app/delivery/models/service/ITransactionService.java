package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Transaction;

public interface ITransactionService {
	
	public List<Transaction> findAll();
	
	public List<Transaction> transactionActive();
	
	public void save(Transaction transaction);
	
	public void delete(Long id);
	
	public Transaction transaction(Long id);

}
