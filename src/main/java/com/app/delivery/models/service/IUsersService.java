package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;

public interface IUsersService {
	
	public Users findByUsername(String username);
	
	public List<Users> findAll();
	
	public List<Users> usersActive();
	
	public void save(Users users);
	
	public void delete(Long id);
	
	public void updateToken(String token, Long id);

}
