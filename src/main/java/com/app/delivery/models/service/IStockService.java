package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Stock;

public interface IStockService {
	
	public List<Stock> findAll();
	
	public List<Stock> stockActive();
	
	public void save(List<Stock> stock);
	
	public void delete(Long id);
	
	public Stock stock(Long id);
	
	public Integer updateStock(Integer count, Long id);

}
