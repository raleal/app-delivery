package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.ICommerceDao;
import com.app.delivery.models.dao.IStockDao;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Stock;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IStockService;

@Service
public class IStockImpl implements IStockService{
	
	@Autowired
	private IStockDao iStockDao;

	@Override
	public List<Stock> findAll() {
		// TODO Auto-generated method stub
		return (List<Stock>) iStockDao.findAll();
	}

	@Override
	public void save(List<Stock> stock) {
		// TODO Auto-generated method stub
		iStockDao.saveAll(stock);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Stock> stockActive() {
		// TODO Auto-generated method stub
		return (List<Stock>) iStockDao.stockActive();
	}

	@Override
	public Stock stock(Long id) {
		// TODO Auto-generated method stub
		return iStockDao.stock(id);
	}

	@Override
	public Integer updateStock(Integer count, Long id) {
		// TODO Auto-generated method stub
		return iStockDao.updateStock(count, id);
		
	}

	

}
