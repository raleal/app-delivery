package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserStock;

public interface IUserStockService {
		
	public List<UserStock> findAll();
	
	public List<UserStock> userStockActive();
	
	public void save(UserStock userStock);
	
	public void delete(Long id);
	
	public void inactiveUserStock(Boolean status, Long id);
	
	public List<UserStock> userStock(Long id);
	
	public UserStock userStockDB(Long id);

}
