package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Brand;

public interface IBrandService {
	
	public List<Brand> findAll();
	
	public List<Brand> brandActive();
	
	public void save(Brand brand);
	
	public void delete(Long id);
	
	public Brand brand(Long id);

}
