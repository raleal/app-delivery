package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Delivery;

public interface IDeliveryService {
	
	public List<Delivery> findAll();
	
	public List<Delivery> deliveryActive();
	
	public void save(Delivery delivery);
	
	public void delete(Long id);
	
	public Delivery delivery(Long id);

}
