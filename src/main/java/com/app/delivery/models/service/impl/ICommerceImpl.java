package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.ICommerceDao;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.service.ICommerceService;

@Service
public class ICommerceImpl implements ICommerceService{
	
	@Autowired
	private ICommerceDao iCommerceDao;

	@Override
	public List<Commerce> findAll() {
		// TODO Auto-generated method stub
		return (List<Commerce>) iCommerceDao.findAll();
	}

	@Override
	public void save(List<Commerce> commerce) {
		// TODO Auto-generated method stub
		iCommerceDao.saveAll(commerce);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Commerce> commerceActive() {
		// TODO Auto-generated method stub
		return (List<Commerce>) iCommerceDao.commerceActive();
	}

	@Override
	public Commerce commerce(Long id) {
		// TODO Auto-generated method stub
		return iCommerceDao.commerce(id);
	}

	

}
