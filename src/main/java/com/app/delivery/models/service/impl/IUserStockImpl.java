package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.ICommerceDao;
import com.app.delivery.models.dao.IUserStockDao;
import com.app.delivery.models.dao.IUsersDao;
import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Users;
import com.app.delivery.models.entity.UserStock;
import com.app.delivery.models.service.ICommerceService;
import com.app.delivery.models.service.IUserStockService;
import com.app.delivery.models.service.IUsersService;

@Service
public class IUserStockImpl implements IUserStockService{
	
	@Autowired
	private IUserStockDao iUserStockDao;
	
	@Override
	public List<UserStock> findAll() {
		// TODO Auto-generated method stub
		return (List<UserStock>) iUserStockDao.findAll();
	}

	@Override
	public void save(UserStock UserStock) {
		// TODO Auto-generated method stub
		iUserStockDao.save(UserStock);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<UserStock> userStockActive() {
		// TODO Auto-generated method stub
		return (List<UserStock>) iUserStockDao.userStockActive();
	}

	@Override
	public void inactiveUserStock(Boolean status, Long id) {
		// TODO Auto-generated method stub
		iUserStockDao.inactiveUserStock(status, id);
	}

	@Override
	public List<UserStock> userStock(Long id) {
		// TODO Auto-generated method stub
		return (List<UserStock>) iUserStockDao.userStock(id);
	}

	@Override
	public UserStock userStockDB(Long id) {
		// TODO Auto-generated method stub
		return iUserStockDao.userStockDB(id);
	}

}
