package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.IBrandDao;
import com.app.delivery.models.entity.Brand;
import com.app.delivery.models.service.IBrandService;

@Service
public class IBrandImpl implements IBrandService{
	
	@Autowired
	private IBrandDao iBrandDao;

	@Override
	public List<Brand> findAll() {
		// TODO Auto-generated method stub
		return (List<Brand>) iBrandDao.findAll();
	}

	@Override
	public void save(Brand brand) {
		// TODO Auto-generated method stub
		iBrandDao.save(brand);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Brand> brandActive() {
		// TODO Auto-generated method stub
		return (List<Brand>) iBrandDao.brandActive();
	}

	@Override
	public Brand brand(Long id) {
		// TODO Auto-generated method stub
		return iBrandDao.brand(id);
	}

	

}
