package com.app.delivery.models.service;

import java.util.List;

import com.app.delivery.models.entity.Commerce;
import com.app.delivery.models.entity.Product;

public interface IProductService {
	
	public List<Product> findAll();
	
	public List<Product> productActive();
	
	public void save(Product product);
	
	public void delete(Long id);
	
	public Product product(Long id);

}
