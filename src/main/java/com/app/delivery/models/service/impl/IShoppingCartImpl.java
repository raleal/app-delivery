package com.app.delivery.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.delivery.models.dao.IShoppingCartDao;
import com.app.delivery.models.entity.ShoppingCart;
import com.app.delivery.models.service.IShoppingCartService;

@Service
public class IShoppingCartImpl implements IShoppingCartService{
	
	@Autowired
	private IShoppingCartDao iShoppingCartDao;

	@Override
	public List<ShoppingCart> findAll() {
		// TODO Auto-generated method stub
		return (List<ShoppingCart>) iShoppingCartDao.findAll();
	}

	@Override
	public void save(List<ShoppingCart> shoppingCart) {
		// TODO Auto-generated method stub
		iShoppingCartDao.saveAll(shoppingCart);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ShoppingCart> shoppingCartActive() {
		// TODO Auto-generated method stub
		return (List<ShoppingCart>) iShoppingCartDao.shoppingCartActive();
	}

	@Override
	public ShoppingCart shoppingCart(Long id) {
		// TODO Auto-generated method stub
		return iShoppingCartDao.shoppingCart(id);
	}

	

}
