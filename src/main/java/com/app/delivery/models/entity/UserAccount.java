package com.app.delivery.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users_account")
public class UserAccount implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String number;

	private String identification;

	private String holder;

	private String phone;

	private Boolean movilPay;
	
	private Boolean status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "updated_date")
    private Date updatedDate;
	
	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="bank_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Bank bank;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="type_identification_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private TypeIdentification typeIdentification;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="type_account_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private TypeAccount typeAccount;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Users users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getNumber() {
		return number;
	}

	public String getIdentification() {
		return identification;
	}

	public String getHolder() {
		return holder;
	}

	public String getPhone() {
		return phone;
	}

	public Boolean getMovilPay() {
		return movilPay;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setMovilPay(Boolean movilPay) {
		this.movilPay = movilPay;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Bank getBank() {
		return bank;
	}

	public TypeIdentification getTypeIdentification() {
		return typeIdentification;
	}

	public TypeAccount getTypeAccount() {
		return typeAccount;
	}

	public Users getUsers() {
		return users;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public void setTypeIdentification(TypeIdentification typeIdentification) {
		this.typeIdentification = typeIdentification;
	}

	public void setTypeAccount(TypeAccount typeAccount) {
		this.typeAccount = typeAccount;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
