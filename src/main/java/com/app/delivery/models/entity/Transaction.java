package com.app.delivery.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Double amounth;	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Users user;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="commerce_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Commerce commerce;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "create_date")
	private Date createDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "update_date")
    private Date updateDate;
	
	@PrePersist
	public void prePersist() {
		createDate = new Date();
	}
	
	@PreUpdate
    public void preUpdate() {
		updateDate = new Date();
    }
	
	private Boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Double getAmounth() {
		return amounth;
	}

	public Users getUser() {
		return user;
	}

	public Commerce getCommerce() {
		return commerce;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setAmounth(Double amounth) {
		this.amounth = amounth;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public void setCommerce(Commerce commerce) {
		this.commerce = commerce;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
