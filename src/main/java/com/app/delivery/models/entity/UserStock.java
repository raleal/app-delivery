package com.app.delivery.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users_stock")
public class UserStock implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer count;
	
	private Boolean status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "updated_date")
    private Date updatedDate;
	
	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="stock_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Stock stock;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="users_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Users users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getCount() {
		return count;
	}

	public Stock getStock() {
		return stock;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
