package com.app.delivery.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "stock")
public class Stock implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer count;

	private Integer content;

	private Double unit_price;
	
	private Boolean status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "updated_date")
    private Date updatedDate;
	
	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Unit unit;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="commerce_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Commerce commerce;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="product_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Product product;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="brand_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Brand brand;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="currency_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Currency currency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getCount() {
		return count;
	}

	public Integer getContent() {
		return content;
	}

	public Double getUnit_price() {
		return unit_price;
	}

	public Unit getUnit() {
		return unit;
	}

	public Commerce getCommerce() {
		return commerce;
	}

	public Product getProduct() {
		return product;
	}

	public Brand getBrand() {
		return brand;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setContent(Integer content) {
		this.content = content;
	}

	public void setUnit_price(Double unit_price) {
		this.unit_price = unit_price;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public void setCommerce(Commerce commerce) {
		this.commerce = commerce;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
