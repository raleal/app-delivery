package com.app.delivery.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "commerce_week")
public class CommerceWeek implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Boolean status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "updated_date")
    private Date updatedDate;
	
	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="commerce_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Commerce commerce;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="week_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Week week;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Commerce getCommerce() {
		return commerce;
	}

	public Week getWeek() {
		return week;
	}

	public void setCommerce(Commerce commerce) {
		this.commerce = commerce;
	}

	public void setWeek(Week week) {
		this.week = week;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
